/**
 * This React class is intended to query an endpoint that will return an alphanumeric string, after clicking a button.
 * This component is passed a prop "apiQueryDelay", which delays the endpoint request by N milliseconds. There is a
 * second button to disable this functionality and have the endpoint request run immediately after button click.
 * This data is then to be displayed inside a simple container.
 * The "queryAPI" XHR handler will return the endpoint response in the form of a Promise (such as axios, fetch).
 * The response object will look like the following: {data: "A0B3HCJ"}
 * The containing element ref isn't used, but should remain within the class.
 * Please identify, correct and comment on any errors or bad practices you see in the React component class below.
 * Additionally, please feel free to change the code style as you see fit.
 * Please note - React version for this exercise is 15.5.4
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import queryAPI from './queryAPI';

const defaultDataState = {data: null, isError: false};

class ShowResultsFromApi extends Component {
  constructor(props) {
    super(props);

    this.state = {
      apiQueryDelay: props.apiQueryDelay,
      ...defaultDataState,
    };

    this.onClick = this.onClick.bind(this);
    this.onDisableDelay = this.onDisableDelay.bind(this);

    this.containerRef = null;
    this.setContainerRef = this.setContainerRef.bind(this);
  }

  onClick() {
    if (this.state.apiQueryDelay) {
      setTimeout(() => this.fetchData(), this.state.apiQueryDelay);
    } else {
      return this.fetchData();
    }
  }

  onDisableDelay() {
    this.setState({
      apiQueryDelay: 0,
      ...defaultDataState,
    });
  }

  fetchData() {
    return queryAPI()
      .then(response =>
        this.setState({
          data: response.data,
          isError: false,
        }),
      )
      .catch(() => this.setState({data: null, isError: true}));
  }

  setContainerRef = element => {
    this.containerRef = element;
  };

  render() {
    const {data, isError} = this.state;
    return (
      <div className="content-container" ref={this.setContainerRef}>
        <p>
          {isError ? 'Sorry - there was an error with your request.' : data}
        </p>

        <button name="disable-delay" onClick={this.onDisableDelay}>
          Disable request delay
        </button>
        <button name="request" onClick={this.onClick}>
          Request data from endpoint
        </button>
      </div>
    );
  }
}

ShowResultsFromApi.displayName = {
  name: 'ShowResultsFromApi',
};
ShowResultsFromApi.defaultProps = {
  apiQueryDelay: 0,
};
ShowResultsFromApi.propTypes = {
  apiQueryDelay: PropTypes.number,
};

export default ShowResultsFromApi;
