import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ShowResultsFromApi from './ShowResultsFromApi';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <ShowResultsFromApi apiQueryDelay={1000} />,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
