import React from 'react';
import {shallow} from 'enzyme';

import ShowResultsFromApi from './ShowResultsFromApi';

import queryAPI from './queryAPI';

jest.mock('./queryAPI');
const mockSuccesfullQueryAPI = () =>
  queryAPI.mockImplementationOnce(
    () => new Promise((resolve, reject) => resolve({data: 'A0B3HCJ'})),
  );
const mockErrorQueryAPI = () =>
  queryAPI.mockImplementationOnce(
    () => new Promise((resolve, reject) => reject()),
  );

describe('<ShowResultsFromApi />', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    queryAPI.mockClear();
  });

  it('renders the content container', () => {
    const wrapper = shallow(<ShowResultsFromApi />);
    expect(wrapper.find('.content-container')).toHaveLength(1);
  });

  it('fetch data when delay is enabled', () => {
    const wrapper = shallow(<ShowResultsFromApi apiQueryDelay={1000} />);
    mockSuccesfullQueryAPI();

    wrapper.find('button[name="request"]').simulate('click');

    expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1000);
  });

  it('fetch data when delay is disabled', () => {
    const wrapper = shallow(<ShowResultsFromApi apiQueryDelay={1000} />);
    mockSuccesfullQueryAPI();

    wrapper.find('button[name="disable-delay"]').simulate('click');
    wrapper.find('button[name="request"]').simulate('click');

    expect(setTimeout).not.toHaveBeenLastCalledWith(expect.any(Function), 1000);
    expect(setTimeout).not.toHaveBeenLastCalledWith(expect.any(Function), 0);
  });

  it('accepts succesfull request', () => {
    const wrapper = shallow(<ShowResultsFromApi apiQueryDelay={0} />);
    mockSuccesfullQueryAPI();

    expect(wrapper.instance().onClick()).not.resolves;
  });

  it('accepts error in request', () => {
    const wrapper = shallow(<ShowResultsFromApi apiQueryDelay={0} />);
    mockErrorQueryAPI();

    expect(wrapper.instance().onClick()).resolves;
  });

  it('renders the request data', () => {
    const wrapper = shallow(<ShowResultsFromApi apiQueryDelay={0} />);

    wrapper.setState({data: 'A0B3HCJ', isError: false});

    expect(wrapper.find('.content-container p').text()).toEqual('A0B3HCJ');
  });

  it('renders the request error', () => {
    const wrapper = shallow(<ShowResultsFromApi apiQueryDelay={0} />);

    wrapper.setState({data: null, isError: true});

    expect(wrapper.find('.content-container p').text()).toEqual(
      'Sorry - there was an error with your request.',
    );
  });
});
