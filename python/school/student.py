from school import Answer, Submission


class Student(object):
    def __init__(self, name=None):
        self.name = name

    def answer_to(self, question, choices):
        answer = Answer.create(question)
        answer.choices = choices
        answer.student = self
        return answer

    def submit_answers(self, answers, assignment):
        submission = Submission.create(answers, assignment)
        assignment.submissions.append(submission)
        return submission

    def grade_term(self, term, all_assignments):
        return sum(
            assignment.grade()
            for assignment in all_assignments
            if assignment.term == term
        )
