class Class(object):
    def __init__(self, name=None):
        self.name = name
        self.students = []

    def enroll(self, student):
        self.students.append(student)
