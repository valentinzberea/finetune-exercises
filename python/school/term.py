class Term(object):
    """The term of a school year

    Usually it can be a semester but depending on the educational system
    it can be also a trimester"""

    def __init__(self, name=None):
        self.name = name
