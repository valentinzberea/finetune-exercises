import pytest
from pytest_factoryboy import LazyFixture

from school import Answer, Assignment, Question, CORRECT_SCORE, WRONG_SCORE


def test_assignment_is_created_with_quiz(new_quiz):
    assignment = Assignment.create(new_quiz)
    assert assignment.quiz == new_quiz


def test_assignment_without_submissions_is_graded_zero(student_assignment):
    assert student_assignment.grade() == 0


def test_assignment_with_empty_submission_is_graded_zero(
    student_assignment, new_student
):
    new_student.submit_answers([], student_assignment)
    assert student_assignment.grade() == 0


def test_assignment_with_wrong_submission_is_graded_zero(
    student_assignment, new_student, correct_choice, wrong_choice
):
    question = Question(choices=[correct_choice, wrong_choice])
    answer = Answer(question=question, choices=[wrong_choice])

    new_student.submit_answers([answer], student_assignment)

    assert student_assignment.grade() == WRONG_SCORE


def test_assignment_with_correct_submission_is_graded(
    student_assignment, new_student, correct_choice, wrong_choice
):
    question = Question(choices=[correct_choice, wrong_choice])
    answer = Answer(question=question, choices=[correct_choice])

    new_student.submit_answers([answer], student_assignment)

    assert student_assignment.grade() == CORRECT_SCORE
