import pytest

from school import Choice, Question


def test_question_created_with_text():
    question = Question(text="question")
    assert question.text == "question"


def test_question_is_created_with_choices(new_choice, another_choice):
    question = Question(choices=[new_choice, another_choice])
    assert question.choices == [new_choice, another_choice]
