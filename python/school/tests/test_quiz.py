import pytest


def test_quiz_is_empty_at_creation(new_quiz):
    assert len(new_quiz.questions) == 0


@pytest.mark.parametrize("new_quiz__name", ["quiz"])
def test_quiz_created_with_name(new_quiz):
    assert new_quiz.name == "quiz"


def test_questions_can_be_added_to_new_quiz(
    new_quiz, new_question, another_question
):
    new_quiz.add_question(new_question)
    new_quiz.add_question(another_question)
    assert len(new_quiz.questions) == 2
