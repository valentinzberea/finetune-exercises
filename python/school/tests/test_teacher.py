import pytest

from school import Teacher


@pytest.mark.parametrize("new_teacher__name", ["A. Being"])
def test_teacher_created_with_name(new_teacher):
    assert new_teacher.name == "A. Being"


def test_teacher_assigns_quiz(new_teacher, new_student, new_quiz):
    assignment = new_teacher.assign_quiz_to(new_student, new_quiz)
    assert assignment.teacher == new_teacher


def test_teacher_assigns_quiz_to_student(new_teacher, new_student, new_quiz):
    assignment = new_teacher.assign_quiz_to(new_student, new_quiz)
    assert assignment.student == new_student
