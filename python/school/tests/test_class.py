def test_class_is_empty_at_creation(new_class):
    assert len(new_class.students) == 0


def test_class_enrolls_students(new_class, new_student, another_student):
    new_class.enroll(new_student)
    new_class.enroll(another_student)
    assert new_class.students == [new_student, another_student]
