import pytest
from pytest_factoryboy import LazyFixture

from school import Answer, Question, CORRECT_SCORE


@pytest.mark.parametrize("new_student__name", ["A. Being"])
def test_student_created_with_name(new_student):
    assert new_student.name == "A. Being"


def test_student_answers_question(
    new_student, new_question, new_choice, another_choice
):
    answer = new_student.answer_to(new_question, [])
    assert answer.student == new_student


def test_student_answers_question_with_no_choice(
    new_student, full_question, new_choice, another_choice
):
    answer = new_student.answer_to(full_question, [])
    assert answer.choices == []


def test_student_answers_question_with_one_choice(
    new_student, full_question, new_choice, another_choice
):
    answer = new_student.answer_to(full_question, [another_choice])
    assert answer.choices == [another_choice]


def test_student_answers_question_with_all_choices(
    new_student, full_question, new_choice, another_choice
):
    answer = new_student.answer_to(full_question, [new_choice, another_choice])
    assert answer.choices == [new_choice, another_choice]


def test_student_submits_answers_to_assignment(
    student_assignment, new_student, new_answer, another_answer
):
    answers = [new_answer, another_answer]
    submission = new_student.submit_answers(answers, student_assignment)
    assert submission.answers == [new_answer, another_answer]


@pytest.mark.parametrize(
    "student_assignment__term", [LazyFixture(lambda term1: term1)]
)
def test_student_is_graded_per_term(
    student_assignment,
    student_assignment_term1,
    student_assignment_term2,
    new_student,
    correct_choice,
    wrong_choice,
    term1,
    term2,
):
    # Submission 1st to an assignment in term 1
    question1 = Question(choices=[correct_choice, wrong_choice])
    answer1 = Answer(question=question1, choices=[correct_choice])
    new_student.submit_answers([answer1], student_assignment)

    # Submission 2nd to an assignment in term 1
    question2 = Question(choices=[correct_choice, wrong_choice])
    answer2 = Answer(question=question2, choices=[correct_choice])
    new_student.submit_answers([answer2], student_assignment_term1)

    # Submission 2nd to an assignment in term 2
    question3 = Question(choices=[correct_choice, wrong_choice])
    answer3 = Answer(question=question3, choices=[correct_choice])
    new_student.submit_answers([answer3], student_assignment_term2)

    all_assignments = [
        student_assignment,
        student_assignment_term1,
        student_assignment_term2,
    ]

    assert new_student.grade_term(term1, all_assignments) == 2 * CORRECT_SCORE
    assert new_student.grade_term(term2, all_assignments) == CORRECT_SCORE
