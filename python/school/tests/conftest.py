from pytest_factoryboy import register, LazyFixture

from school import CORRECT_SCORE, WRONG_SCORE

from school.factories import (
    AnswerFactory,
    AssignmentFactory,
    ChoiceFactory,
    ClassFactory,
    QuestionFactory,
    QuizFactory,
    StudentFactory,
    TeacherFactory,
    TermFactory,
)

register(TeacherFactory, "new_teacher")

register(StudentFactory, "new_student")
register(StudentFactory, "another_student")

register(ClassFactory, "new_class")

register(TermFactory, "term1")
register(TermFactory, "term2")

register(ChoiceFactory, "new_choice")
register(ChoiceFactory, "another_choice")
register(ChoiceFactory, "correct_choice", score=CORRECT_SCORE)
register(ChoiceFactory, "wrong_choice", score=WRONG_SCORE)

register(QuestionFactory, "full_question")
register(QuestionFactory, "new_question")
register(QuestionFactory, "another_question")

register(QuizFactory, "new_quiz")

register(
    AssignmentFactory,
    "student_assignment",
    quiz=LazyFixture("new_quiz"),
    student=LazyFixture("new_student"),
    teacher=LazyFixture("new_teacher"),
    term=LazyFixture("term1"),
)
register(
    AssignmentFactory,
    "student_assignment_term1",
    quiz=LazyFixture("new_quiz"),
    student=LazyFixture("new_student"),
    teacher=LazyFixture("new_teacher"),
    term=LazyFixture("term1"),
)
register(
    AssignmentFactory,
    "student_assignment_term2",
    quiz=LazyFixture("new_quiz"),
    student=LazyFixture("new_student"),
    teacher=LazyFixture("new_teacher"),
    term=LazyFixture("term2"),
)
register(
    AnswerFactory,
    "new_answer",
    student=LazyFixture("new_student"),
    question=LazyFixture("new_question"),
)
register(
    AnswerFactory,
    "another_answer",
    student=LazyFixture("new_student"),
    question=LazyFixture("another_question"),
)
register(
    AnswerFactory,
    "correct_answer",
    student=LazyFixture("new_student"),
    question=LazyFixture("new_question"),
)
register(
    AnswerFactory,
    "wrong_answer",
    student=LazyFixture("new_student"),
    question=LazyFixture("another_question"),
)
