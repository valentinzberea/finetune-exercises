from school import CORRECT_SCORE, WRONG_SCORE


class Answer(object):
    def __init__(self, question=None, choices=[], student=None):
        self.question = question
        self.choices = choices

        self.student = student

    @staticmethod
    def create(question):
        answer = Answer(question)
        return answer

    @property
    def score(self):
        if self.correct_choices == self.choices:
            return CORRECT_SCORE
        else:
            return WRONG_SCORE

    @property
    def correct_choices(self):
        return list(
            choice
            for choice in self.question.choices
            if choice.score == CORRECT_SCORE
        )
