# We say a correct score is a certain value
CORRECT_SCORE = 10
# and a wrong score has the value 0
WRONG_SCORE = 0


class Choice(object):
    def __init__(self, text=None, score=WRONG_SCORE):
        self.text = text
        self.score = score
