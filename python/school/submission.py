class Submission(object):
    """The collection of answers for the student to publish for a quiz"""

    def __init__(self, answers=[], assignment=None):
        self.answers = answers
        self.assignment = assignment

    @staticmethod
    def create(answers, assignment):
        submission = Submission(answers, assignment)
        return submission
