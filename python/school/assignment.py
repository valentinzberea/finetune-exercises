class Assignment(object):
    """An assignment is the quiz that a student has to solve"""

    def __init__(self, quiz=None, student=None, teacher=None, term=None):
        self.quiz = quiz
        self.student = student
        self.teacher = teacher
        self.term = term

        self.submissions = []

    @staticmethod
    def create(quiz):
        assignment = Assignment()
        assignment.quiz = quiz
        return assignment

    def grade(self):
        grade = 0
        if len(self.submissions) == 0:
            return grade

        # The assumption is that the last submission contains
        # all the answers that will be evaluated when we grade
        # the assignment
        last_submission = self.submissions[-1]
        for answer in last_submission.answers:
            grade += answer.score

        return grade
