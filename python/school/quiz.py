class Quiz(object):
    def __init__(self, name=None, questions=[]):
        self.name = name
        self.questions = questions

    def add_question(self, question):
        """Add new question to the set of question in the quiz"""
        self.questions.append(question)
