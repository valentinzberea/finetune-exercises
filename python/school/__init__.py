from importlib import import_module

from .term import Term

from .choice import Choice, CORRECT_SCORE, WRONG_SCORE
from .answer import Answer
from .assignment import Assignment
from .question import Question
from .quiz import Quiz
from .submission import Submission


from .teacher import Teacher
from .student import Student

Class = import_module(".class", "school").Class
