import factory
from faker import Factory as FakerFactory

from school import Term

faker = FakerFactory.create()


class TermFactory(factory.Factory):

    name = factory.LazyAttribute(lambda x: faker.word())

    class Meta:
        model = Term
