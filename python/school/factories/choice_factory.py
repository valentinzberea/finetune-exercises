import factory
from faker import Factory as FakerFactory

from school import Choice

faker = FakerFactory.create()


class ChoiceFactory(factory.Factory):

    text = factory.LazyAttribute(lambda x: faker.sentence(nb_words=3))
    score = factory.LazyAttribute(lambda x: faker.random_digit())

    class Meta:
        model = Choice
