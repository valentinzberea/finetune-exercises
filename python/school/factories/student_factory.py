import factory
from faker import Factory as FakerFactory

from school import Student

faker = FakerFactory.create()


class StudentFactory(factory.Factory):

    name = factory.LazyAttribute(lambda x: faker.name())

    class Meta:
        model = Student
