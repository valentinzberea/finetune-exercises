import factory
from faker import Factory as FakerFactory

from school import Teacher

faker = FakerFactory.create()


class TeacherFactory(factory.Factory):

    name = factory.LazyAttribute(lambda x: faker.name())

    class Meta:
        model = Teacher
