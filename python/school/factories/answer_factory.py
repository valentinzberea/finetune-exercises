import factory
from faker import Factory as FakerFactory

from school import Answer
from school.factories import ChoiceFactory, QuestionFactory, StudentFactory

faker = FakerFactory.create()


class AnswerFactory(factory.Factory):

    student = factory.SubFactory(StudentFactory)
    question = factory.SubFactory(QuestionFactory)

    class Meta:
        model = Answer
