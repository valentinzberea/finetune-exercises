import factory
from faker import Factory as FakerFactory

from school import Quiz

faker = FakerFactory.create()


class QuizFactory(factory.Factory):

    name = factory.LazyAttribute(lambda x: faker.word())

    class Meta:
        model = Quiz
