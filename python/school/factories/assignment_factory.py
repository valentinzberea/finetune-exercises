import factory
from faker import Factory as FakerFactory

from school import Assignment
from school.factories import (
    QuizFactory,
    StudentFactory,
    TeacherFactory,
    TermFactory,
)

faker = FakerFactory.create()


class AssignmentFactory(factory.Factory):

    quiz = factory.SubFactory(QuizFactory)
    student = factory.SubFactory(StudentFactory)
    teacher = factory.SubFactory(TeacherFactory)
    term = factory.SubFactory(TermFactory)

    class Meta:
        model = Assignment
