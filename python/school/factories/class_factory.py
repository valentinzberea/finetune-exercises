import factory
from faker import Factory as FakerFactory

from school import Class

faker = FakerFactory.create()


class ClassFactory(factory.Factory):

    name = factory.LazyAttribute(lambda x: faker.word())

    class Meta:
        model = Class
