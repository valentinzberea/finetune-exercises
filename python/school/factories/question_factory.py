import factory
from faker import Factory as FakerFactory

from school import Question
from school.factories import ChoiceFactory

faker = FakerFactory.create()


class QuestionFactory(factory.Factory):

    code = factory.LazyAttribute(lambda x: faker.word())
    text = factory.LazyAttribute(lambda x: faker.sentence(nb_words=3))

    class Meta:
        model = Question
