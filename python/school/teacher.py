from school import Assignment


class Teacher(object):
    def __init__(self, name=None):
        self.name = name

    def assign_quiz_to(self, student, quiz):
        assignment = Assignment.create(quiz)
        assignment.student = student
        assignment.teacher = self
        return assignment
