Feature: Student solves assignment

  Scenario: Student can solve a new assignment
    Given the Student is signed in
    And the Student sees the list of assignments
    When the Student clicks to solve an assignment
    Then the Student will be prompted to choose choices for all the questions in the quiz

  Scenario: Student can continue solving an existing assignment
    Given the Student is signed in
    And an existing assignment is not graded yet
    When the Student clicks to continue solving an assignment
    Then the Student will be prompted to choose choices for all the questions in the quiz
    And the questions answered before will be marked accordingly
