Feature: Assign quiz

  Scenario: Teacher can assign a quiz to entire class
    When the Teacher choose to assign a quiz to a class
    Then an assignment is created for every Student in the class
    And the term associated with the assignment is the current term of the class

  Scenario: Teacher can assign a quiz to one student in the class
    When the Teacher choose to assign a quiz to a student in her class
    Then an assignment is created for every Student in the class
    And the term associated with the assignment is the current term of the class

  Scenario: Teacher can specify a  multi-choice questions to a quiz
    Given the Teacher has created a quiz
    When the Teacher adds a new multi-choice question
    Then the Teacher can specify the text of the question
    And the Teacher can add multiple choices to the question
    And the Teacher can specify which of the choices are correct
