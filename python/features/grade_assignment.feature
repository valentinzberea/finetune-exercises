Feature: Grade an assignment

  Scenario: Teacher can choose what assignments will be graded
    Given the Teacher selected students
    When the Teacher clicks to grade the assignments
    Then the Teacher will see the final grades for selected students
