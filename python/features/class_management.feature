Feature: Class management

  Scenario: Teacher is notified when new class is assigned
    When a new school year begins
    Then the Teacher is notified when new classes are assigned to her
    And the Teacher receives a link to signin

  Scenario: Teacher can see the list of assigned classes
    Given the Teacher has classes assigned
    When the Teacher signs in the system
    Then the Teacher sees a list of the classes she teaches
    And the Teacher can choose to see the students in a class

  Scenario: Teacher can access a student page to see educational information
    Given the Teacher has a student in her class
    When the Teacher accesses the student's info page
    Then the Teacher sees the student information

  Scenario: Teacher is restricted to access a student's information
    Given the Teacher does not have a student in her class
    When the Teacher accesses the student's info page
    Then the Teacher is restricted to see the student information
