Feature: Grade a full term for a student

  Scenario: Teacher can grade a term for multiple students
    Given the Teacher selected students
    When the Teacher clicks to grade a term
    Then the Teacher will see the final grades for the selected students
