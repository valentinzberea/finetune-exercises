Feature: Create quiz

  Scenario: Teacher can create a new quiz
    When the Teacher starts to create a new quiz
    Then the Teacher can specify the name and description of quiz

  Scenario: Teacher can add multi-choice questions to a quiz
    Given the Teacher has created a quiz
    When the Teacher adds a new multi-choice question
    Then the Teacher can specify the text of the question
    And the Teacher can add multiple choices to the question
    And the Teacher can specify which of the choices are correct
