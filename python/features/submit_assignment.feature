Feature: Student submits assignment

  Scenario: Student can submit an assignment
    Given the Student is signed in
    And the Student sees the list of assignments
    When the Student clicks to submit an assignment
    Then the Student will see that assignment marked accordingly
