1. Please use your most proficient programming language to create object oriented design and use test driven development to implement classes and methods with appropriate data structure and test the code for the following scenario. Please add comments describing any assumptions you make:
2. Please create gherkin test scenarios that cover the following software system. The gherkin scenarios will allow manual testers to run rigorous manual testing and automated testers to write QA automation scripts. Be imaginative since the requirements are not detailed:

- There are Teachers
- There are Students
- Students are in classes that teachers teach
- Teachers can create multiple quizzes with many questions (each question is multiple choice) Teachers can assign quizzes to students
- Students solve/answer questions to complete the quiz, but they don't have to complete it at once. (Partial submissions can be made).
- Quizzes need to get graded
- For each teacher, they can calculate each student's total grade accumulated over a semester for their classes

```mermaid
graph LR
  Teacher("Teacher<br>+name<br>+create_quiz()<br>+assign_quiz_to()")
  Student("Student<br>+name<br>+answer_to()<br>+submit_answers()<br>+grade_term()")
  Class("Class<br>+name<br>+teacher<br>+students<br>+year")
  Year("Year<br>+start_date<br>+end_date<br>+terms")
  Term("Term<br>+name")

  Quiz("Quiz<br>+name<br>+questions")
  Question("Question<br>+code<br>+text<br>+choices")
  Choice("Choice<br>+text<br>+score")

  Assignment("Assignment<br>+teacher<br>+student<br>+quiz<br>+term<br>+submissions<br>+create()<br>+grade()")
  Submission("Submission<br>+assignment<br>+answers<br>+create()")
  Answer("Answer<br>+student<br>+question<br>+choices<br>+submission<br>create()")

  subgraph Assignments
    Answer --> Submission
    Submission --> Assignment
  end

  subgraph Quizes
    Question --> Quiz
    Choice --> Question
  end

  subgraph Classes
    Teacher --> Class
    Student --> Class
    Year --> Class
    Term --> Year
  end
```
